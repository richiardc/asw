/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asw1044;

import java.io.*;
import java.net.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * HTTPClient class
 * 
 * @author rc
 */
public class HTTPClient {

    private URL base = null;
    private String sessionId = null;
    
    /**
     * Get session id 
     * 
     * @return sessionId
     */
    public String getSessionId() {
        return sessionId;
    }
    
    /**
     * Set session id   
     * 
     * @param sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
    /**
     * Set URL  
     * 
     * @param base
     */
    public void setBase(URL base) {
        this.base = base;
    }
    
    /**
     * Get URL 
     * 
     * @return base
     */
    public URL getBase() {
        return base;
    }

    /**
     * Send the document and wait for the reply
     * 
     * @param address
     * @param data
     * @return
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws MalformedURLException 
     */
    public Document execute(String address, Document data) throws TransformerException, ParserConfigurationException, SAXException, IOException, MalformedURLException {
        ManageXML manageXML = new ManageXML();

        HttpURLConnection connection = (HttpURLConnection) new URL(base, address).openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);

        if (sessionId != null) {
            connection.setRequestProperty("Cookie", "JSESSIONID="+sessionId);
        }
        connection.setRequestProperty("Accept", "text/xml");
        connection.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        connection.connect();

        OutputStream out = connection.getOutputStream();
        manageXML.transform(out, data);
        out.close();
        
        InputStream in = connection.getInputStream();
        Document answer = manageXML.parse(in);
        in.close();

        String setCookie = connection.getHeaderField("Set-Cookie");
        if (setCookie != null && !setCookie.equals("") && (setCookie.substring(0, setCookie.indexOf("=")).equals("JSESSIONID"))) {
            sessionId = setCookie.substring(setCookie.indexOf("=")+1, setCookie.indexOf(";"));
        }

        connection.disconnect();
        return answer;
    }

}
