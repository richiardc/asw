/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asw1044;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * ManageXML class
 * 
 * @author rc
 */
public class ManageXML {

    private Transformer transformer;
    private DocumentBuilder builder;
    
    /**
     * ManageXML
     * 
     * @throws TransformerConfigurationException
     * @throws ParserConfigurationException 
     */
    public ManageXML() throws TransformerConfigurationException, ParserConfigurationException {
        transformer = TransformerFactory.newInstance().newTransformer();
        builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    /** 
     * New document creation 
     * 
     * @return document
     */
    public Document newDocument() {
        return builder.newDocument();
    }
    
    /**
     * Process the document 
     * 
     * @param out
     * @param document
     * @throws TransformerException
     * @throws IOException 
     */
    public void transform(OutputStream out, Document document) throws TransformerException, IOException {
        transformer.transform(new DOMSource(document), new StreamResult(out));
    }

    /**
     * Initialize the document with the input info
     * 
     * @param in
     * @return
     * @throws IOException
     * @throws SAXException 
     */
    public Document parse(InputStream in) throws IOException, SAXException {
        return builder.parse(in);
    }
}
