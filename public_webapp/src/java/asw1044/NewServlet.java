package asw1044;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

/**
 * New Scooter Servlet
 * 
 * @author rc
 */
public class NewServlet extends HttpServlet{
    /**
     * Handles the HTTP <code>POST</code> method.<br>
     * Recover the scooter info from the new form and add them to the db
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/new.jsp");
        PrintWriter out= response.getWriter();
        
        // Check the form field
        if ((request.getParameter("id") != null && !request.getParameter("id").isEmpty()) &&
            (request.getParameter("brand") != null && !request.getParameter("brand").isEmpty()) &&
            (request.getParameter("model") != null && !request.getParameter("model").isEmpty()) &&
            (request.getParameter("cc") != null && !request.getParameter("cc").isEmpty()) &&
            (request.getParameter("year") != null && !request.getParameter("year").isEmpty()) &&
            (request.getParameter("km") != null && !request.getParameter("km").isEmpty()) &&
            (request.getParameter("price") != null && !request.getParameter("price").isEmpty())){
            
            String path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/")+"/scooters.xml";
            
            File f = new File(path);

            // Check if db is present
            if(checkDb(f, path)){
                // Scooter db present.
                try {
                    String id = request.getParameter("id");
                    String brand = request.getParameter("brand");
                    String model = request.getParameter("model");
                    String cc = request.getParameter("cc");
                    String year = request.getParameter("year");
                    String km = request.getParameter("km");
                    String price = request.getParameter("price");

                    int added = ScooterManagement.newScooter(id, brand, model, cc, year, km, price, path);

                    if (added == 1){
                        // Scooter added correctly
                        request.setAttribute("scooter_id", id);
                        // I've to add also an image
                        RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/addPhoto.jsp");
                        rd_forward.forward(request, response);

                    } else {
                        out.println("<div align=center><font color=red >Scooter non inserito.</font></div>");
                        rd.include(request, response);
                    }

                } catch (Exception ex) {
                    System.out.println("Error: "+ex);
                    out.println("<div align=center><font color=red >Errore durante l'inserimento dello scooter.</font></div>");
                    rd.include(request, response);
                }

            } else {
                out.println("<div align=center><font color=red >Errore nella lettura del database.</font></div>");
                rd.include(request, response);
            }
        } else {
            // Some fields are empty
            out.println("<div class='new_wrong_form'>Uno o più campi vuoti.<br>Controllare che i campi siano stati inseriti correttamente.</div>");
            rd.include(request, response);
                
        }
    }
    
    /**
     * Method to control if the db already exist
     * 
     * @param f
     * @param path
     * @return 
     */
    private boolean checkDb (File f, String path){
        
        // Check if file is present and is not a directory
        if(f.exists() && !f.isDirectory()){
            // Scooter db present.
            return true;
        } else {
            // Scooter db doesn't exist. I will create it
            try{
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document doc = documentBuilder.newDocument();
                Element root_users = doc.createElement("scooters");
                doc.appendChild(root_users);

                DOMSource source = new DOMSource(doc);

                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                StreamResult result = new StreamResult(path);
                transformer.transform(source, result);
                
                return true;
            }
            catch(Exception ex){
               System.out.println("Error: "+ ex);
               return false;
            }
        }
    }
}

