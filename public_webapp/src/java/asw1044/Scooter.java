/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asw1044;

/**
 
 * Scooter<br>
 * 
 * Scooter info:<br>
 * <ul>
 *      <li>id</li>
 *      <li>brand</li>
 *      <li>model</li>
 *      <li>cc(cilindrata)</li>
 *      <li>year</li>
 *      <li>km</li>
 *      <li>price</li>
 *      <li>image</li>
 * </ul>
 * 
 * 
 * @author rc
 */
public class Scooter {
    
    private String id;
    private String brand;
    private String model;
    private String cc;
    private String year;
    private String km;
    private String price;
    private String image_url;
    
    /**
     * Scooter constructor
     */
    public Scooter(){
        
    }
    
    /**
     * Return scooter's id(matricola)
     * 
     * @return id
     */
    public String getId(){
        return id;
    }
    
    /**
     * Return scooter's brand
     * 
     * @return brand
     */
    public String getBrand(){
        return brand;
    }
    
    /**
     * Return scooter's model
     * 
     * @return model
     */
    public String getModel(){
        return model;
    }
    
    /**
     * Return scooter's displacement(cilindrata)
     * 
     * @return cc
     */
    public String getCc(){
        return cc;
    }
    
    /**
     * Return scooter's year
     * 
     * @return year
     */
    public String getYear(){
        return year;
    }
    
    /**
     * Return scooter's km
     * 
     * @return km 
     */
    public String getKm(){
        return km;
    }
    
    /**
     * Return scooter's price
     * 
     * @return price 
     */
    public String getPrice(){
        return price;
    }
    
    /**
     * Return scooter's image url
     * 
     * @return image_url 
     */
    public String getImage_url(){
        return image_url;
    }
    
    /**
     * Set scooter's id(matricola)
     * 
     * @param id Scooter id
     */
    public void setId(String id){
        this.id = id;
    }
    
    /**
     * Set scooter's brand
     * 
     * @param brand Scooter brand
     */
    public void setBrand(String brand){
        this.brand = brand;
    }
    
    /**
     * Set scooter's model
     * 
     * @param model Scooter model
     */
    public void setModel(String model){
        this.model = model;
    }
    
    /**
     * Set scooter's displacement(cilindrata)
     * 
     * @param cc Scooter cc(cilindrata)
     */
    public void setCc(String cc){
        this.cc = cc;
    }
    
    /**
     * Set scooter's year
     * 
     * @param year Scooter production year
     */
    public void setYear(String year){
        this.year = year;
    }
    
    /**
     * Set scooter's kilometers
     * 
     * @param km Scooter km covered
     */
    public void setKm(String km){
        this.km = km;
    }
    
    /**
     * Set scooter's price
     * 
     * @param price Scooter price
     */
    public void setPrice(String price){
        this.price = price;
    }
    
    /**
     * Set scooter's image url
     * 
     * @param url Scooter photo file name
     */
    public void setImage_url(String url){
        this.image_url = url;
    }
}
