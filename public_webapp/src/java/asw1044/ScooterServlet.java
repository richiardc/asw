package asw1044;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Scooter Servlet
 * 
 * @author rc
 */
public class ScooterServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>GET</code> method.<br>
     * Recover the scooter list
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	try {
                response.setContentType("text/html");
                
                String id = request.getParameter("scooter_id");
                String path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/")+"/scooters.xml";
                
                System.out.println(path);
                // Recover the scooter through its id
                Scooter scooter = ScooterManagement.findScooterById(id,path);

                request.setAttribute("scooter_id", id);
                request.setAttribute("scooter", scooter);
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/modify.jsp");
                rd_forward.forward(request, response);

        } catch (Exception ex) {
                System.out.println("Error: "+ex);
                request.setAttribute("msg", ex.getMessage());
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/error.jsp");
                rd_forward.forward(request, response);
        }
    }
    
}
