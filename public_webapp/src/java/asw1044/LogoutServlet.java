package asw1044;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Logout Servlet
 * 
 * @author rc
 */
public class LogoutServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>POST</code> method.<br>
     * Permit to an admin to logout
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        response.setContentType("text/html");
        // Check all cookies
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                // Look for session id
                if(cookie.getName().equals("JSESSIONID")){
                    break;
                }
            }
        }
        // Invalidate the session if exists
        HttpSession session = request.getSession(false);
        if(session != null){
            session.invalidate();
        }
        // Redirect to home page
        response.sendRedirect("/public_webapp/index.jsp");
    }

}