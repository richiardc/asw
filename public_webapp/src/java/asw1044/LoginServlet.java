package asw1044;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;


/**
 * Login Servlet
 * 
 * @author rc
 */
public class LoginServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>POST</code> method.<br>
     * Check that the request info match with the db
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/login.jsp");
        PrintWriter out= response.getWriter();
        
        // Check if the field isn't empty and initialized
        if ((request.getParameter("admin") != null && !request.getParameter("admin").isEmpty()) && (request.getParameter("pwd") != null && !request.getParameter("pwd").isEmpty())){
            String admin = request.getParameter("admin");
            String pwd = request.getParameter("pwd");
            
            String path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/") +"/admins.xml";
            
            File f = new File(path);
            // Check if file is present and is not a directory
            if(f.exists() && !f.isDirectory()){
                try {
                    // Admin db present. Check the login credentials
                    int user = check_admin(path, admin, pwd);
                    
                    // 1 = admin matched    0 = no match
                    if ( user == 1){ 
                        HttpSession session = request.getSession();
                        session.setAttribute("admin", admin);
                        //setting session to expiry in 30 mins
                        session.setMaxInactiveInterval(30*60);
                        Cookie userName = new Cookie("admin", admin);
                        userName.setMaxAge(30*60);
                        response.addCookie(userName);
                        response.sendRedirect("/public_webapp/ScooterList");
                        
                    }else {
                        out.println("<div class='login_wrong_form'>Non ci sono utenti con queste credenziali.<br>Controlla nuovamente i tuoi dati di login.</div>");
                        rd.include(request, response);
                    }
                } catch (Exception ex) {
                    System.out.println("Error: "+ex);
                    request.setAttribute("msg", "Errore nella lettura del Database");
                    RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/error.jsp");
                    rd_forward.forward(request, response);
                }
            } else {
                // Database not found
                out.println("<div class='login_wrong_form'>Database non trovato</div>");
                rd.include(request, response);  
            }
                
        } else {
            
            out.println("<div class='login_wrong_form'>Username o password vuoti.<br>Controllare che i campi siano stati inseriti correttamente.</div>");
            rd.include(request, response);
        }
    }
    
    /**
     * Method to check user info
     * 
     * @param pathToWrite
     * @param admin
     * @param pwd
     * @return an integer: 0 = failure, 1 = success
     * @throws Exception 
     */
    private static int check_admin(String pathToWrite, String admin, String pwd) throws Exception {
            
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(pathToWrite);
        
        NodeList nList = document.getElementsByTagName("admin");
        
        for (int temp = 0; temp < nList.getLength(); temp++){
            Node nNode = nList.item(temp);
            
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                // Check if username admin is present
                if (eElement.getElementsByTagName("username").item(0).getTextContent().equals(admin)){
                    // Check the password
                    if (eElement.getElementsByTagName("password").item(0).getTextContent().equals(pwd)){
                        // It is an admin!!!
                        return 1;
                    }
                }
            }
        }
        // Check finished without match. Adimn username +/or password not present in db  
        return 0;
        }
}