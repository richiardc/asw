package asw1044;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Modify Scooter Servlet
 * 
 * @author rc
 */
public class ModifyServlet extends HttpServlet{
    /**
     * Handles the HTTP <code>POST</code> method.<br>
     * Recover the scooter info from the new form and add them to the db.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html");
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/jsp/modify.jsp");
        PrintWriter out= response.getWriter();
        
        String path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/")+"/scooters.xml";
        
        // Recover all parameter passed from the form
        String id = request.getParameter("id");
        String brand = request.getParameter("brand");
        String model = request.getParameter("model");
        String cc = request.getParameter("cc");
        String year = request.getParameter("year");
        String km = request.getParameter("km");
        String price = request.getParameter("price");
        String image = request.getParameter("image");
        Scooter scooter = ScooterManagement.findScooterById(id, path);
        
        // Check the modified form field
        if ((request.getParameter("km") != null && !request.getParameter("km").isEmpty()) &&
            (request.getParameter("price") != null && !request.getParameter("price").isEmpty())){
            
            try {
                int modified = ScooterManagement.modifyScooter(id, brand, model, cc, year, km, price, image, path);

                if (modified == 1){
                    // Scooter modified correctly
                    request.setAttribute("scooter_id", id);
                    RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/modified.jsp");
                    rd_forward.forward(request, response);

                } else {
                    request.setAttribute("scooter_id", id);
                    Scooter scooter2 = ScooterManagement.findScooterById(id, path);
                    request.setAttribute("scooter", scooter2);
                    out.println("<div class='new_wrong_form'><font color=red >Scooter non modificato.</font></div>");
                    rd.include(request, response);
                }

            } catch (Exception ex) {
                System.out.println("Error: "+ex);
                out.println("<div align=center><font color=red >Errore durante la modifica dello scooter.</font></div>");
                rd.include(request, response);
            }
        } else {
            // Some or one field are/is empty
            request.setAttribute("scooter_id", id);
            request.setAttribute("scooter", scooter);
            out.println("<div class='new_wrong_form'>Uno o più campi vuoti.<br>Controllare che i campi siano stati modificati correttamente.</div>");
            rd.include(request, response);
                
        }
    }
    
}
