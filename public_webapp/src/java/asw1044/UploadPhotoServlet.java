package asw1044;


import java.io.*;
import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.*;


/**
 * UploadPhoto Servlet<br>
 * Servlet that upload image file to the server
 * 
 * @author rc
 */
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
                 maxFileSize=1024*1024*10,      // 10MB
                 maxRequestSize=1024*1024*50)   // 50MB
public class UploadPhotoServlet extends HttpServlet {
    
    /**
     * Handles the HTTP <code>POST</code> method.<br>
     * Recover the scooter info from the new form and add them to the db
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        
        String photo_path = request.getSession().getServletContext().getRealPath("/media/photos");
        String db_path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/")+"/scooters.xml";
                
        String id = request.getParameter("scooter_id");
        String fileName = id;
        
        try{
            // Receive the image and write it to the server
            Iterator i = request.getParts().iterator();
            
            while(i.hasNext()){
                
               Part p = (Part) i.next();
               
               String cont_type = p.getContentType();
               if (cont_type != null && cont_type.contains("image")) {
                   
                   // Rename the file 
                   String fileExt = cont_type.replaceAll("image/", "");
                   fileName =  fileName + "." + fileExt;
                   
                   File file = new File(photo_path + File.separator + fileName);
                   if (!file.exists()) {
                        file.createNewFile();
                   }
                   
                   InputStream filecontent = null;
                   OutputStream outputStream = new FileOutputStream(file);
                   
                   filecontent = p.getInputStream();
                   
                   int read = 0;
                   byte[] bytes = new byte[1024];

                   while ((read = filecontent.read(bytes)) != -1) {
                        outputStream.write(bytes, 0, read);
                   }
                   
                   outputStream.close();
               }
            }
            
            // Add the file to the db
            int added = ScooterManagement.addImage(id,fileName, db_path);
            
            if(added == 1){
                request.setAttribute("msg", "Operazione eseguita correttamente!");
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/added.jsp");
                rd_forward.forward(request, response);
            }else{
                request.setAttribute("msg", "Errore durante l'upload");
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/error.jsp");
                rd_forward.forward(request, response); 
            }
        } catch (Exception ex) {
           System.out.println("Error: "+ex);
           request.setAttribute("msg", "Errore durante l'upload");
           RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/error.jsp");
           rd_forward.forward(request, response);
        } 
    }
}
