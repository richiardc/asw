package asw1044;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * SearchServlet
 * 
 * @author rc
 */
public class SearchServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>POST</code> method.<br>
     * It takes the parameter sent from the SearchApplet parsing it with ManageXML. After that it search a scooter match on the db.<br>
     * It receive from the ScooterManagement an anwser xml file and send it back to the applet. 
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        
        try {
                    System.out.println("Request arrived");
                        
                    InputStream is = request.getInputStream();
                    response.setContentType("text/xml;charset=UTF-8");
                    
                    ManageXML mngXML = new ManageXML();
                    Document data = mngXML.parse(is);
                    is.close();

                    // Recover parameters from document data sent by applet
                    String path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/")+"/scooters.xml";
                    System.out.println("Path: " +  path);
                    String[] parameters = recoverParam(data);
                    System.out.println("Parameters: " +  parameters);
                    Document search_result = ScooterManagement.searchWithParam(path,parameters);
                    
                    // Send to applet
                    OutputStream os = response.getOutputStream();
                    mngXML.transform(os, search_result);
                    os.close();
                    
                    System.out.println("Servlet work done");
        
        }catch (Exception ex) {
                System.out.println("Error: "+ex);
        }
    }
    
    /*
    * Recover parameter method
    */
    private String[] recoverParam (Document doc){
            
        NodeList parameters = doc.getElementsByTagName("search");
            
        String parameters_list[] = new String[4];

        for (int i = 0; i < parameters.getLength(); i++){
            Node node = parameters.item(i);
            NodeList params = node.getChildNodes();

            for (int k = 0; k < params.getLength(); k++){
                if ("brand".equals(params.item(k).getNodeName())){
                    parameters_list[0] = params.item(k).getTextContent();   
                } else if ("model".equals(params.item(k).getNodeName())){
                    parameters_list[1] = params.item(k).getTextContent();
                } else if ("cc".equals(params.item(k).getNodeName())){
                    parameters_list[2] = params.item(k).getTextContent();
                } else if ("price".equals(params.item(k).getNodeName())){
                    parameters_list[3] = params.item(k).getTextContent();
                }
            }
        }

        return parameters_list;
    }

}
