package asw1044;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

/**
 * ScooterManagement
 *
 * @author rc
 */
public class ScooterManagement {
    
    /**
     * ScooterManagement constructor
     */
    private ScooterManagement(){
        
    }

    /**
     * Return all scooter contained in scooter xml db
     * 
     * @param path Db path
     * @return scooter_list
     */
    public static ArrayList<Scooter> findScooters (String path){
                
        final ArrayList<Scooter> scooter_list = new ArrayList<Scooter>();

        try{
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

                DefaultHandler handler;
                handler = new DefaultHandler() {
                
                // Flags to parse XML db
                boolean scooter_id = false;
                boolean scooter_brand = false;
                boolean scooter_model = false;
                boolean scooter_cc = false;
                boolean scooter_year = false;
                boolean scooter_km = false;
                boolean scooter_price = false;
                boolean scooter_image = false;

                Scooter scooter = null;

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    
                    if (qName.equals("scooter")){
                        scooter = new Scooter();
                    } 
                    if (qName.equals("id")){
                        scooter_id = true;
                    } 
                    if (qName.equals("brand")){
                        scooter_brand = true;
                    } 
                    if (qName.equals("model")){
                        scooter_model = true;
                    } 
                    if (qName.equals("cc")){
                        scooter_cc = true;
                    } 
                    if (qName.equals("year")){
                        scooter_year = true;
                    } 
                    if (qName.equals("km")){
                        scooter_km = true;
                    } 
                    if (qName.equals("price")){
                        scooter_price = true;
                    } 
                    if (qName.equals("image")){
                        scooter_image = true;
                    }
                }

                @Override
                public void characters(char ch[], int start, int length) throws SAXException {
                    
                    if (scooter_id) {
                        scooter.setId(new String(ch, start, length));
                        scooter_id = false;
                    }
                    if (scooter_brand){
                        scooter.setBrand(new String(ch, start, length));
                        scooter_brand = false;
                    }
                    if (scooter_model){
                        scooter.setModel(new String(ch, start, length));
                        scooter_model = false;
                    }
                    if (scooter_cc){
                        scooter.setCc(new String(ch, start, length));
                        scooter_cc = false;
                    }
                    if (scooter_year){
                        scooter.setYear(new String(ch, start, length));
                        scooter_year = false;
                        
                    }
                    if (scooter_km){
                        scooter.setKm(new String(ch, start, length));
                        scooter_km = false;
                    }
                    if (scooter_price){
                        scooter.setPrice(new String(ch, start, length));
                        scooter_price = false;
                    }
                    if (scooter_image){
                        scooter.setImage_url(new String(ch, start, length));
                        scooter_image = false;
                    }
                }

                @Override
                public void endElement(String uri, String localName,String qName) throws SAXException {
                    
                    // if find an end element scooter and scooter object isn't null add the object to the list 
                    if (qName.equals("scooter")){
                        if(scooter != null){
                            scooter_list.add(scooter);
                            scooter = null;
                        }
                    }
                }

            };

            saxParser.parse(path, handler);

        } catch (ParserConfigurationException | SAXException | IOException ex) {
          System.out.println("Error: "+ ex);
        }

        return scooter_list;
    }
    
    /**
     * Return a scooter object through its id
     * 
     * @param id Scooter id
     * @param path Db path
     * @return scooter
     */
     public static Scooter findScooterById (String id, String path){
        
        ArrayList<Scooter> scooter_list = findScooters(path);
        
        // After the parsing look for the exact scooter
        Scooter scooter_found = new Scooter();
        for (Scooter sc : scooter_list) {
            if (sc.getId().equals(id)) {
                scooter_found.setId(sc.getId());
                scooter_found.setBrand(sc.getBrand());
                scooter_found.setModel(sc.getModel());
                scooter_found.setCc(sc.getCc());
                scooter_found.setYear(sc.getYear());
                scooter_found.setKm(sc.getKm());
                scooter_found.setPrice(sc.getPrice());
                scooter_found.setImage_url(sc.getImage_url());
            }
        }
   
        return scooter_found;
    }

     /**
     * Retutn the xml file with scooters found.<br>
     * For each scooter found we save:
     * <ul>
     *  <li>Id</li>
     *  <li>Brand</li>
     *  <li>Model</li>
     *  <li>CC</li>
     *  <li>Year</li>
     *  <li>Price</li>
     * </ul>
     * 
     * @param path Db path
     * @param params Search criteria
     * @return document
     * @throws ParserConfigurationException
     */
    public static Document searchWithParam(String path, String[] params) throws ParserConfigurationException{
        
        ArrayList<Scooter> scooter_list = new ArrayList<Scooter>();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        try{
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

                DefaultHandler handler;
                handler = new DefaultHandler() {
                
                // Flags to parse XML db
                boolean scooter_id = false;
                boolean scooter_brand = false;
                boolean scooter_model = false;
                boolean scooter_cc = false;
                boolean scooter_year = false;
                boolean scooter_km = false;
                boolean scooter_price = false;
                boolean scooter_image = false;
                boolean skip_scooter = false;

                Scooter scooter = null;

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    
                    if (qName.equals("scooter")){
                        scooter = new Scooter();
                        skip_scooter = false;
                    } 
                    if (qName.equals("id") && !skip_scooter){
                        scooter_id = true;
                    } 
                    if (qName.equals("brand") && !skip_scooter){
                        scooter_brand = true;
                    } 
                    if (qName.equals("model") && !skip_scooter){
                        scooter_model = true;
                    } 
                    if (qName.equals("cc") && !skip_scooter){
                        scooter_cc = true;
                    } 
                    if (qName.equals("year") && !skip_scooter){
                        scooter_year = true;
                    } 
                    if (qName.equals("km") && !skip_scooter){
                        scooter_km = true;
                    } 
                    if (qName.equals("price") && !skip_scooter){
                        scooter_price = true;
                    } 
                    if (qName.equals("image") && !skip_scooter){
                        scooter_image = true;
                    }
                }

                @Override
                public void characters(char ch[], int start, int length) throws SAXException {
                    
                    if (scooter_id) {
                        scooter.setId(new String(ch, start, length));
                        scooter_id = false;
                    }
                    if (scooter_brand){
                        // Check the parameter
                        String db_brand = new String(ch, start, length);
                        if(!params[0].equals("-") && !db_brand.equals(params[0])){
                            skip_scooter = true;
                        }
                        scooter.setBrand(new String(ch, start, length));
                        scooter_brand = false;
                    }
                    if (scooter_model){
                        // Check the parameter
                        String db_model = new String(ch, start, length);
                        if(!params[1].equals("") && !db_model.equals(params[1])){
                            skip_scooter = true;
                        }
                        scooter.setModel(new String(ch, start, length));
                        scooter_model = false;
                    }
                    if (scooter_cc){
                        // Check the parameter
                        String db_cc = new String(ch, start, length);
                        if(!params[2].equals("-") && !db_cc.equals(params[2])){
                            skip_scooter = true;
                        }
                        scooter.setCc(new String(ch, start, length));
                        scooter_cc = false;
                    }
                    if (scooter_year){
                        scooter.setYear(new String(ch, start, length));
                        scooter_year = false;
                        
                    }
                    if (scooter_km){
                        scooter.setKm(new String(ch, start, length));
                        scooter_km = false;
                    }
                    if (scooter_price){
                        // Check the parameter
                        int db_price = Integer.parseInt(new String(ch, start, length));
                        int price_param = (!params[3].equals("")) ? Integer.parseInt(params[3]) : 99999;
                        if(db_price > price_param){
                            skip_scooter = true;
                        }
                        scooter.setPrice(new String(ch, start, length));
                        scooter_price = false;
                    }
                    if (scooter_image){
                        scooter.setImage_url(new String(ch, start, length));
                        scooter_image = false;
                    }
                }

                @Override
                public void endElement(String uri, String localName,String qName) throws SAXException {
                    
                    // if find an end element scooter and scooter object isn't null add the object to the list 
                    if (qName.equals("scooter")){
                        if(!skip_scooter && scooter != null){
                            scooter_list.add(scooter);
                            scooter = null;
                        }
                    }
                }

            };

            saxParser.parse(path, handler);
 
        // Create xml document 
        Element root = document.createElement("results");
        document.appendChild(root);
        // Fill all results into document
        for(Scooter sc : scooter_list){
            //This method creates an element node
            Element root_scooter = document.createElement("scooter");
            root.appendChild(root_scooter);
            // ID
            Element id_element = document.createElement("id");
            root_scooter.appendChild(id_element);
            Text scooter_id = document.createTextNode(sc.getId());
            id_element.appendChild(scooter_id);
            // BRAND
            Element brand_element = document.createElement("brand");
            root_scooter.appendChild(brand_element);
            Text scooter_brand = document.createTextNode(sc.getBrand());
            brand_element.appendChild(scooter_brand);
            // MODEL
            Element model_element = document.createElement("model");
            root_scooter.appendChild(model_element);
            Text scooter_model = document.createTextNode(sc.getModel());
            model_element.appendChild(scooter_model);
            // CC
            Element cc_element = document.createElement("cc");
            root_scooter.appendChild(cc_element);
            Text scooter_cc = document.createTextNode(sc.getCc());
            cc_element.appendChild(scooter_cc);
            // YEAR
            Element year_element = document.createElement("year");
            root_scooter.appendChild(year_element);
            Text scooter_year = document.createTextNode(sc.getYear());
            year_element.appendChild(scooter_year);
            // KM
            Element km_element = document.createElement("km");
            root_scooter.appendChild(km_element);
            Text scooter_km = document.createTextNode(sc.getKm());
            km_element.appendChild(scooter_km);
            // PRICE
            Element price_element = document.createElement("price");
            root_scooter.appendChild(price_element);
            Text scooter_price = document.createTextNode(sc.getPrice());
            price_element.appendChild(scooter_price); 
        }
        
        
        } catch (ParserConfigurationException | SAXException | IOException ex) {
          System.out.println("Error: "+ ex);
        }
        return document;
        
    }
     
     
    /**
     * Insert a new scooter into the db taking its info from the form
     *
     * @param id Scooter id
     * @param brand Scooter brand
     * @param model Scooter model
     * @param cc Scooter cc
     * @param year Scooter year
     * @param km Scooter km
     * @param price Scooter price
     * @param path Db path
     * @return an integer: 0 = failure, 1 = success
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws TransformerException 
     */
    public static int newScooter(String id, String brand, String model, String cc, String year, String km, String price, String path) throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException{
        
        try{
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(path);

            Element root = document.getDocumentElement();

            //This method creates an element node
            Element root_scooter = document.createElement("scooter");
            root.appendChild(root_scooter);
            // ID
            Element id_element = document.createElement("id");
            root_scooter.appendChild(id_element);
            Text scooter_id = document.createTextNode(id);
            id_element.appendChild(scooter_id);
            // BRAND
            Element brand_element = document.createElement("brand");
            root_scooter.appendChild(brand_element);
            Text scooter_brand = document.createTextNode(brand);
            brand_element.appendChild(scooter_brand);
            // MODEL
            Element model_element = document.createElement("model");
            root_scooter.appendChild(model_element);
            Text scooter_model = document.createTextNode(model);
            model_element.appendChild(scooter_model);
            // CC
            Element cc_element = document.createElement("cc");
            root_scooter.appendChild(cc_element);
            Text scooter_cc = document.createTextNode(cc);
            cc_element.appendChild(scooter_cc);
            // YEAR
            Element year_element = document.createElement("year");
            root_scooter.appendChild(year_element);
            Text scooter_year = document.createTextNode(year);
            year_element.appendChild(scooter_year);
            // KM
            Element km_element = document.createElement("km");
            root_scooter.appendChild(km_element);
            Text scooter_km = document.createTextNode(km);
            km_element.appendChild(scooter_km);
            // PRICE
            Element price_element = document.createElement("price");
            root_scooter.appendChild(price_element);
            Text scooter_price = document.createTextNode(price);
            price_element.appendChild(scooter_price);


            DOMSource source = new DOMSource(document);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            StreamResult result = new StreamResult(path);
            transformer.transform(source, result);
            
            return 1;
            
        } catch (Exception ex){
            System.out.println(ex);
            return 0;
        }
    }
    
    /**
     * Add to the db the image file_name
     * 
     * @param id Scooter id
     * @param file_name Scooter photo file name
     * @param path Db path
     * @return an integer: 0 = failure, 1 = success
     * @throws ParserConfigurationException 
     * @throws SAXException 
     * @throws IOException 
     * @throws TransformerConfigurationException 
     */
    public static int addImage(String id, String file_name, String path)throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException{
        
        try{
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(path);

            // Take all scooter element
            NodeList scooters = document.getElementsByTagName("scooter");
            
            // Search the scooter from id
            for (int i = 0; i < scooters.getLength(); i++){
                Node node = scooters.item(i);
                NodeList scooter_list = node.getChildNodes();
                boolean find = false;
                int index = -1;
                for (int k = 0; k < scooter_list.getLength(); k++){
                    if ("id".equals(scooter_list.item(k).getNodeName())){
                        if (scooter_list.item(k).getTextContent().equals(id)){
                            find = true;
                            index = i;
                            break;
                        }else{
                            break;
                        }
                    }
                }
                
                // Add on the db the element image
                if (find){
                    Element img = document.createElement("image");
                    scooters.item(index).appendChild(img);
                    Text text_img = document.createTextNode(file_name);
                    img.appendChild(text_img);
                    
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    DOMSource source = new DOMSource(document);
                    StreamResult result = new StreamResult(new File(path));
                    transformer.transform(source, result);
                    
                    return 1;
                } 
            }
        
        } catch (Exception ex){
            System.out.println(ex);
            return 0;
        }    
        return 0;
    }
    
    /**
     * Modify a scooter into the db taking its info from the form
     *
     * @param id Scooter id
     * @param brand Scooter brand
     * @param model Scooter model
     * @param cc Scooter cc(cilindrata)
     * @param year Scooter production year
     * @param km Scooter km covered
     * @param price Scooter price
     * @param image_url Scooter photo file name
     * @param path Db path
     * @return an integer: 0 = failure, 1 = success
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws TransformerException 
     */
    public static int modifyScooter(String id, String brand, String model, String cc, String year, String km, String price, String image_url, String path) throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException{
        
        try{
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(path);
           
            // Take all scooter element
            NodeList scooters = document.getElementsByTagName("scooter");
            
            // Searche the scooter from id
            for (int i = 0; i < scooters.getLength(); i++){
                Node node = scooters.item(i);
                NodeList scooter_list = node.getChildNodes();
                boolean find = false;
                for (int k = 0; k < scooter_list.getLength(); k++){
                    if ("id".equals(scooter_list.item(k).getNodeName())){
                        if (scooter_list.item(k).getTextContent().equals(id)){
                            find = true;
                            break;
                        } else {
                            break;
                        }
                    }
                }
                // Found?!
                if (find){
                    for(int j = 0; j < scooter_list.getLength(); j++){
                        Node n = scooter_list.item(j);
                        if ("id".equals(n.getNodeName())){
                            n.setTextContent(id);
                        } else if ("brand".equals(n.getNodeName())){
                            n.setTextContent(brand);
                        } else if ("model".equals(n.getNodeName())){
                            n.setTextContent(model);
                        } else if ("cc".equals(n.getNodeName())){
                            n.setTextContent(cc);
                        } else if ("year".equals(n.getNodeName())){
                            n.setTextContent(year);
                        } else if ("km".equals(n.getNodeName())){
                            n.setTextContent(km);
                        } else if ("price".equals(n.getNodeName())){
                            n.setTextContent(price);
                        } else if ("image".equals(n.getNodeName())){
                            n.setTextContent(image_url);
                        }
                    }
                    
                    DOMSource source = new DOMSource(document);
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    StreamResult result = new StreamResult(new File(path));
                    transformer.transform(source, result);   
                    
                    return 1;
                }
            }
        } catch (Exception ex){
            System.out.println(ex);
            return 0;
        }
        return 0;
    }
 
    
    /**
     * Delete a scooter from the db using its id
     * 
     * @param path Db path
     * @param image_folder Server scooter image folder
     * @param id Scooter id
     * @param image_to_delete Scooter photo file name
     * @return an integer: 0 = failure, 1 = success
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws TransformerException 
     */
    public static int deleteScooter(String path, String image_folder, String id, String image_to_delete)throws ParserConfigurationException, SAXException, IOException, TransformerConfigurationException, TransformerException {
        
        try{
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(path);
            
            Element root = document.getDocumentElement();
           
            // Take all scooter element
            NodeList scooters = document.getElementsByTagName("scooter");
            
            // Search the scooter from id
            for (int i = 0; i < scooters.getLength(); i++){
                Node node = scooters.item(i);
                NodeList scooter_list = node.getChildNodes();
                boolean find = false;
                for (int k = 0; k < scooter_list.getLength(); k++){
                    if ("id".equals(scooter_list.item(k).getNodeName())){
                        if (scooter_list.item(k).getTextContent().equals(id)){
                            find = true;
                            break;
                        } else {
                            break;
                        }
                    }
                }
                // Found?!
                if (find){
                    
                    root.removeChild(node);
                    
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    DOMSource source = new DOMSource(document);
                    StreamResult result = new StreamResult(new File(path));
                    transformer.transform(source, result);
                    
                    // Remove also the image file from media/photos
                    boolean deleted;
                    File image_file = new File(image_folder + File.separator + image_to_delete);
                    if(image_file.exists()){
                        deleted = image_file.delete();
                        if(deleted){
                            return 1;
                        }   
                    }else{
                        return 1;
                    }
                }
            }
        } catch (Exception ex){
            System.out.println(ex);
            return 0;
        }
        return 0;
    }
    
}