package asw1044;

/**
 * Admin<br>
 * 
 * Admin info:<br>
 * <ul>
 *      <li>user_name</li>
 *      <li>password</li>
 *      <li>name</li>
 * </ul>
 * 
 * @author rc
 */
public class Admin {
    
    private String user_name;
    private String password;
    private String name;

    /**
     * Return Admin's user_name
     * 
     * @return user_name
     */
    public String getUser_name() {
        return user_name;
    }

    /**
     * Return Admin's password
     * 
     * @return password
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * Return Admin's name
     * 
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * Set Admin's user_name
     * 
     * @param user_name User name
     */
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    /**
     * Set Admin's password
     * 
     * @param password User password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Set Admin's name
     * 
     * @param name Admin name
     */
    public void setName(String name) {
        this.name = name;
    
    }
}
