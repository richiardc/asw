package asw1044;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 * ScooterList Servlet
 *
 * @author rc
 */
public class ScooterListServlet extends HttpServlet {
    
    
    /**
     * Handles the HTTP <code>GET</code> method.<br>
     * Recover the scooter list
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	try {
                response.setContentType("text/html");

                String path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/")+"/scooters.xml";
                
                // Recover all scooter list 
                ArrayList<Scooter> scooters_list = ScooterManagement.findScooters(path);

                request.setAttribute("scooters", scooters_list);
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/scooterlist.jsp");
                rd_forward.forward(request, response);

        } catch (Exception ex) {
                System.out.println("Error: "+ex);
                request.setAttribute("msg", ex.getMessage());
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/error.jsp");
                rd_forward.forward(request, response);
        }
    }
}