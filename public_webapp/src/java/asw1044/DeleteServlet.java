package asw1044;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Delete Scooter Servlet
 * 
 * @author rc
 */
public class DeleteServlet extends HttpServlet {
    
    
    /**
     * Handles the HTTP <code>GET</code> method.<br>
     * Delete the scooter through its id
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            
            String id_to_delete = request.getParameter("scooter_id");
            String image_file_name = request.getParameter("image");
            
            String path = request.getSession().getServletContext().getRealPath("/WEB-INF/xml/")+"/scooters.xml";
            String image_folder = request.getSession().getServletContext().getRealPath("/media/photos");
            
            int deleted = ScooterManagement.deleteScooter(path,image_folder, id_to_delete, image_file_name);

            if (deleted == 1){
                // Scooter deleted
                request.setAttribute("msg", "Scooter deleted correctly");
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/deleted.jsp");
                rd_forward.forward(request, response);
            } else {
                // Scooter not deleted or an error was occurred
                request.setAttribute("msg", "Scooter not deleted or an error was occurred");
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/error.jsp");
                rd_forward.forward(request, response);
            }
        } catch (Exception ex) {
                System.out.println("Error: "+ex);
                request.setAttribute("msg", ex.getMessage());
                RequestDispatcher rd_forward = getServletContext().getRequestDispatcher("/jsp/error.jsp");
                rd_forward.forward(request, response);
        }
    }
}
    

