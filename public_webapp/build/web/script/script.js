/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var main = function() {
  /* Push the body and the nav over by 250px over */
  $('.icon-menu').click(function() {
    $('.menu').animate({
      left: "0px"
    }, 200);

    $('body').animate({
      left: "250px"
    }, 200);
    
    $('.icon-login').animate({
        left: "-250px"
    }, 200);
  });

  /* Then push them back */
  $('.icon-close').click(function() {
    $('.menu').animate({
      left: "-250px"
    }, 200);

    $('body').animate({
      left: "0px"
    }, 200);
    
    $('.icon-login').animate({
        left: "0px"
    }, 200);
  });
  
  /* Sliding information on the home page */
  $("#menu").accordion({collapsible: true, active: false, heightStyle: "content"});
  
};

$(document).ready(main);


