<%-- 
    Document   : contacts
    Created on : 6-nov-2015, 13.41.38
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        <title>Contacts Page</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/basic_menu.jspf" %>
        <br>
        <div class="row vertically_centered">
            <div class="first_col">
                <div class="google-map-container">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1431.2290890612196!2d12.240401849999992!3d44.1564152!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132ca5267d0504ed%3A0x8b433482972f1da3!2sBarberini+Snc+Di+Andrea+Barberin%2C+Appennini%2C+Via+Enzo+Ferrari%2C+110%2C+Cesena%2C+FC!5e0!3m2!1sit!2sit!4v1391132930729" width="600" height="450" frameborder="0" style="border:0"></iframe>
                </div>
            </div>
            <div class="second_col">
                <div class="contact-informations">
                    <div><h4>Barberini Snc Di Andrea Barberini</h4></div>
                    <div>
                        <h4>Sede Cesena</h4>
                            <ul>
                                <li><span>via E. Ferrari, 110</span></li>
                                <li><span>Cesena, FC 47521, ITA</span></li>
                                <li><span>+39 0547 380777</span></li>
                                <li><span><a class="link" href="mailto:info@barberinimoto.com">info@barberinimoto.com</span></p></li>
                            </ul>
                    </div>
                    <div class="contact-informations hours">
                        <h4>Orari Cesena</h4>
                        <ul >
                            <li>Lun - Ven <span>8.30 -12.30 15-19</span></li>
                            <li>Sab <span>8.30 -12.30</span></li>
                            <li>Dom <span>Chiuso</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
 
    </body>
</html>
