<%-- 
    Document   : error
    Created on : 7-nov-2015, 13.52.49
    Author     : rc
--%>

<%
boolean isAdmin = false;
if(session.getAttribute("admin") != null){
    isAdmin = true;
}   
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Error Page</title>
    </head>
    <body>
        <%if(!isAdmin){%>
                <%@ include file="/WEB-INF/jspf/basic_menu.jspf" %>
            <%} else {%>
                <%@ include file="/WEB-INF/jspf/admin_menu.jspf" %>
            <%}%>
            <div class="advise_form">
                <div class="text_very_big">F*CK! C'è stato un errore!</div>
                <br>
                <br>
                <br>
                <%if(isAdmin){%>
                    <a class="button" href="/public_webapp/ScooterList">Ritorna alla lista scooter</a>
                <%} else { %>
                    <a class="button" href="/public_webapp/index.jsp">Ritorna alla Home</a>
                <%}%>
            </div>
    </body>
</html>
