<%-- 
    Document   : addPhoto
    Created on : 30-nov-2015, 11.37.07
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    String id = "";
    if (request.getAttribute("scooter_id") != null) {
        id = (String) request.getAttribute("scooter_id");
    }
 %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Add New Scooter Photo</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/admin_menu.jspf" %>
        <br>
        <br>
        <div class="centered text_very_big">
            Scooter inserito correttamente. Aggiungici una foto!
        </div>
        <div class="photo_form">
            <form action="/public_webapp/UploadPhoto" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="scooter_id" value="<%= id %>">
                <input type="file" name="file">
                <input class="button" type="submit" value="AGGIUNGI">
            </form>
        </div>   
    </body>
</html>