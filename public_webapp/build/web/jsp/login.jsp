<%-- 
    Document   : login
    Created on : 6-nov-2015, 13.41.05
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Login Page</title>
    </head>
        <body>
            <%@ include file="/WEB-INF/jspf/basic_menu.jspf" %>
            <div class ="login_form">
                <form action="/public_webapp/Login" method="POST">
                    USERNAME<br>
                    <input type="text" name="admin">
                    <br>
                    PASSWORD<br>
                    <input type="password" name="pwd">
                    <br>
                    <input class="button" type="submit" value="ACCEDI">
                </form>
            </div>
        </body>
</html>
