<%-- 
    Document   : newscooter
    Created on : 19-nov-2015, 11.29.49
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Add New Scooter</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/admin_menu.jspf" %>
        <br>
        <br>
        <div class="centered text_very_big">
            Nuovo Scooter
        </div>
        <div class="new_form">
            <form action="/public_webapp/NewScooter" method="POST">
                ID<br>
                <input type="text" name="id" >
                <br>
                MARCA<br>
                <input type="text" name="brand">
                <br>
                MODELLO<br>
                <input type="text" name="model">
                <br>
                CC<br>
                <input type="text" name="cc">
                <br>
                ANNO<br>
                <input type="text" name="year">
                <br>
                KM<br>
                <input type="text" name="km">
                <br>
                PREZZO<br>
                <input type="text" name="price">
                <br>
                <input class="button" type="submit" value="INSERISCI">
            </form>
        </div>   
    </body>
</html>
