<%-- 
    Document   : search
    Created on : 16-dic-2015, 15.57.18
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        <title>Search a scooter</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/basic_menu.jspf" %>
        <br>
        <br>
        <br>
        <div class="centered vertically_search_centered">
            <div class="text_very_big">Inserisci i dati per la ricerca dello scooter</div>
            <br>
            <applet code="asw1044.SearchScooter" archive="/public_webapp/applet/applet2.jar, /public_webapp/applet/lib/lib2.jar" width="700" height="600"/>
        </div>
        </div>
</html>
