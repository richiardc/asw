<%-- 
    Document   : scooterlist
    Created on : 16-nov-2015, 12.21.27
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="asw1044.Scooter"%>
<%@page import="asw1044.ScooterManagement"%>
<%@page import="java.util.ArrayList"%>


<%
boolean isAdmin = false;
if(session.getAttribute("admin") != null){
    isAdmin = true;
}   
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link href='/public_webapp/style_sheet/gallery_style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Scooter list</title>
    </head>
    <body>
        <% if (isAdmin){ %>
            <%@ include file="/WEB-INF/jspf/admin_menu.jspf" %>
        <% } else { %>
            <%@ include file="/WEB-INF/jspf/basic_menu.jspf" %>
        <% } %>
        
        <% 
            ArrayList<Scooter> scooter_list = new ArrayList<Scooter>();
            if (request.getAttribute("scooters") != null) {
                scooter_list = (ArrayList<Scooter>) request.getAttribute("scooters");
            }
            
            if (scooter_list.size() == 0){
                
            %>
            <div class="advise_form">
                <div class="text_very_big">Al momento non vi sono scooter usati in vendita</div>
                <br>
                <br>
                <br>
                <% if(isAdmin){ %>
                    <a class="button" href="/public_webapp/jsp/new.jsp">Inserisci uno scooter</a>
                <% }else{ %>
                    <a class="button" href="/public_webapp/index.jsp">Ritorna alla Home</a>
                <% } %>
            </div>
            <%
                
            } else {
                
            %>
            
            <br>
            <br>
            <div class="welcome centered text_very_big">
                Scooter usati in vendita
            </div> 
            
            <div>
            <ul class="rig columns-2 centered">
            
            <%
            // Recover all scooters data     
            for(int i = 0; i < scooter_list.size(); i++){
                    String id = scooter_list.get(i).getId();
                    String brand = scooter_list.get(i).getBrand();
                    String model = scooter_list.get(i).getModel();
                    String cc = scooter_list.get(i).getCc();
                    String year = scooter_list.get(i).getYear();
                    String km = scooter_list.get(i).getKm();
                    String price = scooter_list.get(i).getPrice();
                    String image_url = scooter_list.get(i).getImage_url();
                    
                    // Formatting some scooter data
                    String id_to_handle = id;
                    id = "ID: " + id;
                    cc = cc + " cc";
                    year = "Anno di immatricolazione: " + year;
                    km = "Km: " + km;
                    price = "Prezzo: " + price + " Euro";
                    String image_file_name = image_url;
                    image_url = "/public_webapp/media/photos/" + image_url; 
                    
                    %>
                    <li>
                        <img src="<%=image_url%>"/>
                        <h3><%=brand%> <%=model%> <%=cc%></h3>
                        <p><%=id%></p>
                        <p><%=year%></p>
                        <p><%=km%></p>
                        <p class="price"><%=price%></p>
                        <% 
                        // I'll add the tool to modify ad delete a scooter insertion 
                        if(isAdmin){ %>
                        <div class="tools">
                            <a onclick="alert('Vuoi davvero modificare lo scooter?')" href='/public_webapp/Scooter?scooter_id=<%= id_to_handle %>'><img id="icon_tools" src="/public_webapp/media/modify.png"/></a>
                            &nbsp;&nbsp;&nbsp;
                            <a onclick="alert('Confermi la cancellazione dello scooter?')" href='/public_webapp/DeleteScooter?scooter_id=<%= id_to_handle %>&&image=<%=image_file_name%>'><img id="icon_tools" src="/public_webapp/media/delete.png"/></a>
                        </div>
                        <% } %>
                    </li>
            <%
             } %>
                    
                       
            <%
            }
            %>
            </ul>
            </div>
    </body>
</html>
