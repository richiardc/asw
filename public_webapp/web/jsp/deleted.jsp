<%-- 
    Document   : deleted
    Created on : 20-nov-2015, 22.23.02
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Scooter deleted</title>
    </head>
    <body>
            <%@ include file="/WEB-INF/jspf/admin_menu.jspf" %>
            <div class="advise_form">
                <div class="text_very_big">Lo scooter è stato cancellato correttamente!</div>
                <br>
                <br>
                <br>
                <a class="button" href="/public_webapp/ScooterList">Ritorna alla lista scooter</a>
            </div>
    </body>
</html>
