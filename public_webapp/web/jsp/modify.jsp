<%-- 
    Document   : modify
    Created on : 1-dic-2015, 21.37.02
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="asw1044.Scooter"%>
<%@page import="asw1044.ScooterManagement"%>

<%
    String id = "";
    if (request.getAttribute("scooter_id") != null) {
        id = (String) request.getAttribute("scooter_id");
    }

    Scooter sc = new Scooter();
    if (request.getAttribute("scooter") != null) {
        sc = (Scooter) request.getAttribute("scooter");
    }
            
 %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Modify Scooter</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/jspf/admin_menu.jspf" %>
        <br>
        <br>
        <div class="centered text_very_big">
            Modifica km e prezzo dello scooter
        </div>
        <div class="new_form">
            <form action="/public_webapp/ModifyScooter" method="POST">
                ID<br>
                <input type="text" name="show_id" value="<%= id %>" disabled="true">
                <br>
                MARCA<br>
                <input type="text" name="show_brand" value="<%= sc.getBrand() %>" disabled="true">
                <br>
                MODELLO<br>
                <input type="text" name="show_model" value="<%= sc.getModel() %>" disabled="true">
                <br>
                CC<br>
                <input type="text" name="show_cc" value="<%= sc.getCc() %>" disabled="true">
                <br>
                ANNO<br>
                <input type="text" name="show_year" value="<%= sc.getYear() %>" disabled="true">
                <br>
                KM<br>
                <input type="text" name="km" value="<%= sc.getKm() %>" >
                <br>
                PREZZO<br>
                <input type="text" name="price" value="<%= sc.getPrice() %>">
                <br>
                <input type="hidden" name="id" value="<%= id %>">
                <input type="hidden" name="brand" value="<%= sc.getBrand() %>">
                <input type="hidden" name="model" value="<%= sc.getModel() %>">
                <input type="hidden" name="cc" value="<%= sc.getCc() %>">
                <input type="hidden" name="year" value="<%= sc.getYear() %>">
                <input type="hidden" name="image" value="<%= sc.getImage_url() %>">
                <input type="hidden" name="scooter" value="<%= sc %>"
                <br>
                <input class="button" type="submit"  value="MODIFICA">
            </form>
        </div>   
    </body>
</html>

