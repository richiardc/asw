<%-- 
    Document   : index
    Created on : 6-nov-2015, 11.49.18
    Author     : rc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 
    <head>
        <link href='/public_webapp/style_sheet/style.css' rel='stylesheet'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="/public_webapp/script/script.js"></script>
        
        <title>Find your scooter</title>
    </head>

  <body>

    <%@ include file="/WEB-INF/jspf/basic_menu.jspf" %>
    
    <br>
    <br>
    
    <div class="welcome centered text_very_big">
            Benvenuto nella sezione scooter usati!
        </div>
        <div class="welcome centered text_big">In questo portale potrai cercare lo scooter dei tuoi desideri tra le tante offerte presenti al momento. E' tutto molto semplice, dal menù che ritrovi in alto a sinistra potrai:</div>
        <div id="menu">
            <h3>Visualizzare tutti i nostri scooter usati in vendita</h3>
            <div>
                <p>Non sai di preciso cosa stai cercando o non hai ancora alcuna preferenza sul modello da ricercare?! Non ti preoccupare potrai visualizzare tutte le nostre offerte.<br>Ti abbiamo messo a disposizione l'elenco completo degli scooter attualmente in vendita presso la nostra sede. Non ti rimane che darci un'occhiata.</p>
            </div>
            <h3>Cercare lo scooter che fa per te</h3>
            <div>
                <p>Potrai realizzare una ricerca tra tutte le offerte presenti in sede utilizzando i parametri di ricerca che più identificano cosa stai realmente cercando.<br> Potrai personalizzare la tua ricerca attraverso diversi paramentri come ad esempio marca, cilindrata, prezzo, anno di immatricolazione e così via..</p>
            </div>
            <h3>Prenotare una prova su strada</h3>
            <div>
                <p>Se hai trovato un modello che ti può interessare e sei voglioso di provarlo prima di un eventuale acquisto non preoccuparti, tramite il portale potrai contattarci direttamente ed inserendo solo pochi dati invierai una mail al nostro responsabile, il quale cercherà di risponderti il prima possibile.</p>
            </div>
            <h3>Trovare tutte le informazioni per contattarci</h3>
            <div>
                <p>Sono presenti pure tutte le informazioni necessarie per conoscere gli orari di apertura, i contatti telefonici e pure una mappa che ti indica il posto esatto in cui ci troviamo. In poche parole sai tutto di noi!</p>
            </div>
        </div>
        <div class="foot centered text_big">- Progetto di Applicazioni e Servizi Web realizzato da Richiard Casadei -</div>
        <br>
  </body>
</html>
