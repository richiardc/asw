package asw1044;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.swing.*;
import static javax.swing.GroupLayout.Alignment.*;


/**
 * BookService class<br>
 * Applet that permits to send an e-mail to booking a test drive of the requested scooter.<br>
 * The e-mail will be sent from an gmail account (asw1044test@gmail.com) to an another mail.
 * 
 * @author rc
 */
public class BookService  extends JApplet implements ActionListener {
    
   private final String morningString = "09:30 - 10:30";
   private final String afternoonString = "15:00 - 16:00";
   private final String eveningString = "18:30 - 19:30";
   private final String noString = "Nessuna preferenza";
   
   private String mailmessage;
   
   // Labels and text areas.
   private JLabel label1;
   private JTextField contact;
   private JLabel label2;
   private JTextField id;
   private JLabel label3;
   
   private JLabel sent;
   
   // Radio buttons.
   private JCheckBox morning;
   private JCheckBox afternoon;
   private JCheckBox evening;
   private JCheckBox no;
   
   // Send button
   private JButton send;
   private JButton cancel;
   
   // Mail settings
   private final int port = 587;
   private final String host = "smtp.gmail.com";
   private final String from = "aswtest1044@gmail.com";
   private final String to = "shadow674rc@gmail.com";
   private final boolean auth = true;
   private final String username = "aswtest1044@gmail.com";
   private final String password = "aswpassword";
   private final boolean debug = true;
   
   private Thread mailthread = null;
   private SendMailTask smt = null;
   
   @Override
   public void init(){
       
       label1 = new JLabel("Inserisci e-mail e/o num. tel. :");
       label1.setFont(new Font("OpenSans", Font.PLAIN, 13));
       contact = new JTextField(20);
       label2 = new JLabel("Inserisci l'ID dello scooter:");
       label2.setFont(new Font("OpenSans", Font.PLAIN, 13));
       id = new JTextField(20);
       label3 = new JLabel("Seleziona l'orario preferito");
       label3.setFont(new Font("OpenSans", Font.PLAIN, 13));
       
       sent = new JLabel("La richiesta è stata inviata correttamente.");
       sent.setFont(new Font("OpenSans", Font.PLAIN, 13));
       sent.setVisible(false);
       
       // Time checkbox
       morning = new JCheckBox(morningString);
       afternoon = new JCheckBox(afternoonString);
       evening = new JCheckBox(eveningString);
       no = new JCheckBox(noString);

       // Remove redundant default border of check boxes
       morning.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
       afternoon.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
       evening.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
       no.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
       
       // Button
       send = new JButton("Invia Richiesta");
       cancel = new JButton("Cancella");

       GroupLayout layout = new GroupLayout(getContentPane());
       getContentPane().setLayout(layout);
       layout.setAutoCreateGaps(true);
       layout.setAutoCreateContainerGaps(true);
       getContentPane().setBackground(Color.decode("#EEEEEE"));

       // Set horizontal position
       layout.setHorizontalGroup(
            layout.createSequentialGroup()
               .addGroup(layout.createParallelGroup(LEADING)
                   .addComponent(label1)
                   .addComponent(label2)
                   .addComponent(label3))
               .addGroup(layout.createParallelGroup(LEADING)
                   .addComponent(contact)
                   .addComponent(id)
                   .addGroup(layout.createParallelGroup(LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(morning)
                            .addComponent(afternoon))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(evening)
                            .addComponent(no)))
                    .addComponent(sent))
               .addGroup(layout.createParallelGroup()
                   .addComponent(send)
                   .addComponent(cancel))   
                   
       );
       
       layout.linkSize(SwingConstants.HORIZONTAL, send, cancel);

       // Set vertical position
       layout.setVerticalGroup(
            layout.createSequentialGroup()
               .addGroup(layout.createParallelGroup(BASELINE)
                   .addComponent(label1)
                   .addComponent(contact)
                   .addComponent(send))
               .addGroup(layout.createParallelGroup(BASELINE)
                   .addComponent(label2)
                   .addComponent(id)
                   .addComponent(cancel))
               .addGroup(layout.createParallelGroup(LEADING)
                   .addComponent(label3)
                   .addGroup(layout.createSequentialGroup())    
                        .addGroup(layout.createParallelGroup()
                            .addComponent(morning)
                            .addComponent(afternoon)))
                    .addGroup(layout.createSequentialGroup())
                        .addGroup(layout.createParallelGroup()
                                .addComponent(evening)
                                .addComponent(no))
               .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED,10, Short.MAX_VALUE)    
               .addComponent(sent)
               
       );
       
       // Add button listeners
       send.addActionListener(this);
       cancel.addActionListener(this);
       
   }
    
   @Override
    public void actionPerformed(ActionEvent e) {
	   
        Object source = e.getSource();

        // Send button
        if(source ==  send){

            sent.setVisible(false);
            
            // Check time preference
            int time_selected = 0;
            String time = "";

            if (morning.isSelected()) {
                time_selected++;
                if(time_selected > 1){
                    time += " " + morningString;
                } else {
                    time += morningString;
                }
            }
            if(afternoon.isSelected()){
                time_selected++;
                if(time_selected > 1){
                    time += " " + afternoonString;
                } else {
                    time += afternoonString; 
                }
            }
            if(evening.isSelected()){
                time_selected++;
                if(time_selected > 1){
                    time += " " + eveningString;
                } else {
                    time += eveningString;
                }
            }
            if(no.isSelected()){
                time_selected++;
                if(time_selected > 1){
                    time += " " + noString;
                } else {
                    time += noString;
                }
            }
            if(time.equals("")){
                time += "Non specificata";
            }
            
            // Start thread and send mail 
            if(mailthread != null){
                System.out.println("Thread not null");
                smt.terminate();
            }else{
                SendMailTask smt = new SendMailTask(time);
                mailthread = new Thread(smt);
                mailthread.start();
                mailthread = null;
            }
            
        } else {
            // Cancel button. Reset all fiels
            contact.setText("");
            id.setText("");
            morning.setSelected(false);
            afternoon.setSelected(false);
            evening.setSelected(false);
            no.setSelected(false);
            sent.setVisible(false);
        }
   }
    
    /**
     * Send mail task for the thread
     */
    public class SendMailTask implements Runnable {
        
        private String time;
        private Boolean terminate = false;
        
        public SendMailTask(String time) {
            
            this.time = time;
        }
        
        public void terminate(){
            this.terminate = true;
        }
        
        @Override
        public void run() {
            try{
                
                if(!terminate){
                    // Set mail message to send
                    mailmessage = "Si richiede una prenotazione per una prova su strada dello scooter " + id.getText() + " nella/e finestra/e oraria/e: " + time + ". \n Prego cortesemente di esser contattato a: "+contact.getText();

                    // Set properties and auth
                    Properties props = new Properties();  
                    props.put("mail.smtp.host", host);  
                    props.put("mail.smtp.socketFactory.port", port);  
                    props.put("mail.smtp.starttls.enable", "true");
                    props.put("mail.smtp.auth", auth);  
                    props.put("mail.smtp.port", port);  

                    Authenticator aut = new javax.mail.Authenticator() {  
                        protected PasswordAuthentication getPasswordAuthentication() {  
                            return new PasswordAuthentication(username,password);  
                        }  
                    };

                    Session session = Session.getDefaultInstance(props, aut);
                    session.setDebug(debug);

                    // Define message
                    MimeMessage message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(from));
                    Address address = new InternetAddress(to);
                    message.addRecipient(Message.RecipientType.TO, address);
                    message.setSubject("Richiesta prova su strada dello scooter " + id.getText());
                    message.setText(mailmessage);

                    // Send mail message
                    Transport.send(message);

                    System.out.println("La richiesta è stata inviata!");
                    sent.setVisible(true);
                    contact.setText("");
                    id.setText("");
                    morning.setSelected(false);
                    afternoon.setSelected(false);
                    evening.setSelected(false);
                    no.setSelected(false);
                }

            }catch(Exception ex){
                ex.printStackTrace();
                
                // Check what kind of exception is
                if (ex instanceof SendFailedException) {
                    SendFailedException sfex = (SendFailedException)ex;
                    Address[] invalid = sfex.getInvalidAddresses();
                    if (invalid != null) {
                        System.out.println("    ** Invalid Addresses");
                        for (int i = 0; i < invalid.length; i++) 
                            System.out.println("         " + invalid[i]);
                    }
                    Address[] validUnsent = sfex.getValidUnsentAddresses();
                    if (validUnsent != null) {
                        System.out.println("    ** ValidUnsent Addresses");
                        for (int i = 0; i < validUnsent.length; i++) 
                            System.out.println("         "+validUnsent[i]);
                    }
                    Address[] validSent = sfex.getValidSentAddresses();
                    if (validSent != null) {
                        System.out.println("    ** ValidSent Addresses");
                        for (int i = 0; i < validSent.length; i++) 
                            System.out.println("         "+validSent[i]);
                    }
                }
                if (ex instanceof MessagingException){
                    ex = ((MessagingException)ex).getNextException();
                }else{
                    ex = null;
                }
            }
        }
    }
}
