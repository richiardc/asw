package asw1044;

import asw1044.HTTPClient;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;
import javax.swing.*;
import static javax.swing.GroupLayout.Alignment.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.*;
import javax.xml.transform.TransformerException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * SearchScooter class.<br>
 * Applet used to search a scooter.<br>
 * A user can search a scooter inserting some parameter(brand, model,cc and price) and the applet ask to the SearchServlet if one scooter or more matches with the parameter.
 * 
 * @author rc
 */
public class SearchScooter extends JApplet implements ActionListener{
    
    private final String[] brands = {"-", "Aprilia", "Gilera", "Honda", "Yamaha", "Kymco", "Piaggio", "Pegeout", "Suzuki", "Vespa"};
    private final String[] ccs = {"-", "50", "125", "150", "250", "300", "500", "600"};
    
    private JLabel brand;
    private JLabel model;
    private JLabel cc;
    private JLabel price;
    
    private JComboBox brand_list;
    private JComboBox cc_list;
    
    private JTextField model_text;
    private JTextField price_text;
    
    private JButton find;
    private JButton cancel;
    
    private ScooterTableModel tablemodel;
    private JTable result;
    private String[] columnNames = {"Id", "Marca", "Modello", "CC", "Year", "Km", "Prezzo"};
    private String[][] data; //= {{"","","","","","",""}}; 
    
    private Thread searchTread = null;
    private SearchTask st = null;
    private String  contextPath;
    private JScrollPane scroll_pane;
    
    /**
    * Init method
    */
    @Override
    public void init() {

        contextPath = "/public_webapp";
        
        brand = new JLabel("Marca: ");
        brand.setFont(new Font("OpenSans", Font.PLAIN, 13));
        brand_list = new JComboBox(brands);
        
        model = new JLabel("Modello: ");
        model.setFont(new Font("OpenSans", Font.PLAIN, 13));
        model_text = new JTextField(20);
        
        cc = new JLabel("Cilindrata: ");
        cc.setFont(new Font("OpenSans", Font.PLAIN, 13));
        cc_list = new JComboBox(ccs);
        
        price = new JLabel("Prezzo (Max):  ");
        price.setFont(new Font("OpenSans", Font.PLAIN, 13));
        price_text = new JTextField(20);
        
        find = new JButton("Ricerca");
        cancel = new JButton("Cancella");
        
        find.addActionListener(this);
        cancel.addActionListener(this);
        
        tablemodel = new ScooterTableModel(data, columnNames);
        result = new JTable();
        result.setModel(tablemodel);
        result.setPreferredScrollableViewportSize(new Dimension(700, 200));
        result.setFillsViewportHeight(true);
        scroll_pane = new JScrollPane(result);
        
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        getContentPane().setBackground(Color.decode("#EEEEEE"));
       
        // Set horizontal position
        layout.setHorizontalGroup(
             layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(LEADING)
                    .addComponent(brand)
                    .addComponent(model)
                    .addComponent(cc)
                    .addComponent(price))   
                .addGroup(layout.createParallelGroup(LEADING)
                    .addComponent(brand_list)
                    .addComponent(model_text)
                    .addComponent(cc_list)
                    .addComponent(price_text)
                    .addGroup(layout.createParallelGroup(CENTER)    
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(find)
                            .addComponent(cancel))
                    .addComponent(scroll_pane)))
        );  

        // Set vertical position
        layout.setVerticalGroup( 
             layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(BASELINE)
                    .addComponent(brand)
                    .addComponent(brand_list))
                .addGroup(layout.createParallelGroup(BASELINE)
                    .addComponent(model)
                    .addComponent(model_text))
                .addGroup(layout.createParallelGroup(BASELINE)
                    .addComponent(cc)
                    .addComponent(cc_list))
                .addGroup(layout.createParallelGroup(BASELINE)
                    .addComponent(price)
                    .addComponent(price_text))    
                .addGroup(layout.createSequentialGroup()
                     .addGroup(layout.createParallelGroup(BASELINE)
                        .addComponent(find)
                        .addComponent(cancel)))
                .addComponent(scroll_pane)
        );
           
    }
     
    @Override
    public void actionPerformed(ActionEvent e) {
        
        Object source = e.getSource();
        
        if(source == cancel){
            
            // Reset al param field
            brand_list.setSelectedIndex(0);
            model_text.setText("");
            cc_list.setSelectedIndex(0);
            price_text.setText("");
            
            // Clean table
            tablemodel.setRowCount(0);
            tablemodel.fireTableDataChanged();
            
        }else if(source == find){  
        
            // Clean table
            tablemodel.setRowCount(0);
            tablemodel.fireTableDataChanged();
            
            // Recover all the parameters for the research
            String brand_param = (String)brand_list.getSelectedItem();
            String model_param = model_text.getText();
            String cc_param = (String)cc_list.getSelectedItem();
            String price_param = price_text.getText();
            
            if(searchTread != null){
                System.out.println("Thread not null");
                st.terminate();
            }else{
                // Create search task passing the parameters
                System.out.println("Thread null! Create a new once");
                st = new SearchTask(brand_param, model_param, cc_param, price_param);
                searchTread = new Thread(st);
                searchTread.start();
                searchTread = null;
            }
        }
    }
    
    /**
     * It creates an request xml file 
     * 
     * @param brand_param
     * @param model_param
     * @param cc_param
     * @param price_param
     * @return
     * @throws ParserConfigurationException 
     */
    private Document createXMLRequest(String brand_param, String model_param, String cc_param, String price_param) throws ParserConfigurationException{
            
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbf.newDocumentBuilder();
        Document doc = builder.newDocument();
        // Create the root element node called search
        Element search = doc.createElement("search");
        doc.appendChild(search);
        
        // Create the brand element
        Element brand_el = doc.createElement("brand");
        search.appendChild(brand_el);
        Text brand_text = doc.createTextNode(brand_param);
        brand_el.appendChild(brand_text);
        // Create the model element
        Element model_el = doc.createElement("model");
        search.appendChild(model_el);
        Text model_eltext = doc.createTextNode(model_param);
        model_el.appendChild(model_eltext);
        // Create the cc element
        Element cc_el = doc.createElement("cc");
        search.appendChild(cc_el);
        Text cc_text = doc.createTextNode(cc_param);
        cc_el.appendChild(cc_text);
        // Create the cc element
        Element price_el = doc.createElement("price");
        search.appendChild(price_el);
        Text price_eltext = doc.createTextNode(price_param);
        price_el.appendChild(price_eltext);

        return doc;
    }
    
    /**
     * Recover the search result data from servlet answer document
     * 
     * @param doc
     * @return 
     */
    private ArrayList<String[]> recoverAnswerFromDoc(Document doc){
        
        ArrayList<String[]> answer = new ArrayList<String[]>();
        
        // Take all scooter element
        NodeList scooters = doc.getElementsByTagName("scooter");
        
        for (int i = 0; i < scooters.getLength(); i++){
                Node node = scooters.item(i);
                NodeList scooter_list = node.getChildNodes();
                String[] info = new String[scooter_list.getLength()];
                for (int j = 0; j < scooter_list.getLength();j++){
                   info[j] = scooter_list.item(j).getTextContent();
                }
                answer.add(info);
        }
        
        return answer;
    }
    
    /**
     * Search Task for the thread
     */
    public class SearchTask implements Runnable {
        
        private String brand;
        private String model;
        private String cc;
        private String price;
        private Boolean terminate = false;
        
        public SearchTask(String brand_param, String model_param, String cc_param, String price_param) {
            
            this.brand = brand_param;
            this.model = model_param;
            this.cc = cc_param;
            this.price = price_param;
        }
        
        public void terminate(){
            this.terminate = true;
        }

        @Override
        public void run() {
            try{
                // The 3 if(!terminate) check the 3 important phase of the search because user could send another search request 
                // #1
                if(!terminate){
                
                    // Send data to servlet
                    HTTPClient client_request = new HTTPClient();
                    String servlet_adress = getCodeBase().getProtocol() + "://" + getCodeBase().getHost() + ":" + getCodeBase().getPort() + contextPath + "/Search";
                    System.out.println("Servlet addr: " + servlet_adress);
                    Document request = createXMLRequest(brand, model, cc, price);
                    System.out.println("Request created");
                    
                    // #2
                    if(!terminate){
                        Document doc = client_request.execute(servlet_adress, request);
                        System.out.println("Answer received");

                        // Received document result from servlet, recover the answers
                        ArrayList<String[]> answers = recoverAnswerFromDoc(doc);
                        System.out.println("Result recovered");
                        System.out.println("Answer size: " + answers.size());
                        
                        // No scooter found
                        if(answers.size() == 0){
                            tablemodel.setRowCount(3);
                            String[] noresulttext = {"","","Nessun", "scooter", "trovato.", "",""};
                            tablemodel.addRow(noresulttext);
                            tablemodel.fireTableDataChanged();
                            
                        }else{
                        
                            // #3
                            if(!terminate){
                                for(int j=0; j < answers.size(); j++){
                                    String[] info = answers.get(j);
                                    String[] result = new String[7];
                                    System.out.println("Array di stringa:" + Arrays.toString(info));
                                    for(int k=0; k < answers.get(j).length; k++){
                                        result[k] =  info[k]; 
                                    }
                                    tablemodel.addRow(result);
                                }
                                // Update the table
                                tablemodel.fireTableDataChanged();
                                System.out.println("Table ugraded");
                            }
                        }
                    }
                }
            }catch(ParserConfigurationException | TransformerException | SAXException | IOException ex){
                ex.printStackTrace();
            }
        }
    }
    
    /**
     * Custom table model
     */
    public class ScooterTableModel extends DefaultTableModel{
            
        public ScooterTableModel(Object[][] data, Object[] columnNames){
            super(data,columnNames);
        }

        public boolean isCellEditable(int row, int col){
            return false;
        }

        public Class getColumnClass(int column){

            return Object.class;
        }
    }
    

}
